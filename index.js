// console.log("forever cult");

let trainer = {

	name: 'Ben Tennyson',
	age: 10,
	pokemon: ['Haunter', 'Machamp', 'Feraligatr', 'Blastoise'],
	friends: {
		kanto: ['Gwen', 'Kevin'],
		hoenn: ['Rook', 'Ester']

	},
	battleTalk: function (lineup) {
		console.log(`It's hero time! ${this.pokemon[lineup]} I choose you!`);

	}
};

console.log(trainer);

trainer.battleTalk(0);
trainer.battleTalk(2);

function pokemonStats (pokeName, currentLevel) {
	this.name = pokeName,
	this.level = currentLevel,
	this.health = currentLevel * 3,
	this.attack = currentLevel * 1.5,
	this.tackle = function (opponent) {
		console.log(`${this.name} tackled ${opponent.name}!`);

	},
	this.faint = function () {
		console.log(`${this.name} has fainted!`);
		
	}
}

let mamoswine = new pokemonStats("Mamoswine", 29);
console.log(mamoswine);

let amarouge = new pokemonStats("Amarouge", 35);
console.log(amarouge);

let decidueye = new pokemonStats("Decidueye", 41);
console.log(decidueye);

let slowbro = new pokemonStats("Slowbro", 18);
console.log(slowbro);

mamoswine.tackle(slowbro);
slowbro.faint();

decidueye.tackle(amarouge);
amarouge.tackle(decidueye);
decidueye.tackle(amarouge);
amarouge.faint();






